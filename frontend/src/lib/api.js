async function requestGETCommon(path, session) {
    const f = await fetch(`http://127.0.0.1:5000${path}`, {
        headers: {
            "Cookie": `session_id=${session}`
        }
    });
    return await f.text();
}

async function requestPOSTJson(path, session, body) {
    body['session_id'] = session;
    const f = await fetch(`http://127.0.0.1:5000/${path}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Cookie": `session_id=${session}`
        },
        body: JSON.stringify(body)
    });

    console.log(f);

    return await f.json();
}

async function requestPOSTCommon(path, session, body) {
    body['session_id'] = session;
    const f = await fetch(`http://127.0.0.1:5000/${path}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Cookie": `session_id=${session}`
        },
        body: JSON.stringify(body)
    });
    return await f.text();
}

async function loadQuizList(session) {
    const resp = await requestGETCommon("/api/quiz/all", session);
    return JSON.parse(resp);
}

async function loadQuiz(session, uuid) {
    const resp = await requestGETCommon(`/api/quiz/${uuid}`, session);
    return JSON.parse(resp);
}

async function loadQuestion(session, uuid) {
    const resp = await requestGETCommon(`/api/question/${uuid}`, session);
    return JSON.parse(resp);
}

async function loadQuizQuestion(session, uuid, n) {
    const resp = await requestGETCommon(`/api/quiz/${uuid}/question/${n}`, session);
    return JSON.parse(resp);
}

async function loadQuizQuestions(session, quizID) {
    const resp = await requestGETCommon(`/api/quiz/${quizID}`, session);
    return JSON.parse(resp);
}

async function updateQuiz(session, id, data) {
    const resp = await requestPOSTCommon(`/api/quiz/${id}/update`, session, data);
    return JSON.parse(resp);
}

// Returns the UUID of the created quiz
async function createQuiz(session) {
    const uuid = await requestPOSTCommon(`/api/quiz/create`, session, {});
    return uuid;
}

async function submitQuiz(session, id, data) {
    const resp = await requestPOSTCommon(`/api/quiz/${id}/verify`, session, data);
    return resp;
}

async function register(username, password) {
    const r = await requestPOSTJson(`/api/register`, "", {
        username: username,
        password: password
    });

    if(!r['success']) return false;

    return r['user_id'];
}


async function login(username, password) {
    const r = await requestPOSTJson(`/api/login`, "", {
        username: username,
        password: password
    });

    if(!r['success']) return false;

    return r['session_id'];
}

async function loggedin(session) {
    const r = await requestPOSTJson(`/api/loggedin`, session, {});
    return r['success'];
}


export default {
    loadQuizList,
    loadQuizQuestion,
    loadQuizQuestions,
    loadQuiz,
    createQuiz,
    updateQuiz,
    submitQuiz,
    login,
    register,
    loggedin
};
