import API from "$lib/api"

export async function load({ cookies, params }) {
	return {
        quizData: await API.loadQuiz(cookies.get("session"), params.id),
        questionData: await API.loadQuizQuestion(cookies.get("session"), params.id, params.qid),
        questionNumber: params.qid
    };
}
