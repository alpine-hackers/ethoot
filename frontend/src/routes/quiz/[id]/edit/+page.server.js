import API from "$lib/api"

export async function load({ cookies, params }) {
	return {
        quizData: await API.loadQuiz(cookies.get("session"), params.id),
        questionsData: await API.loadQuizQuestions(cookies.get("session"), params.id)
    };
}
