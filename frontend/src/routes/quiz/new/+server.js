import API from "$lib/api";
import { fail, redirect } from "@sveltejs/kit";

export async function GET({ cookies }) {
    if(!await API.loggedin(cookies.get("session"))){
        throw redirect(302, `/login`);
    }

    const uuid = await API.createQuiz(cookies.get("session"));
    if (!uuid) {
        throw fail(500, "Failed to create quiz");
    }
    throw redirect(302, `/quiz/${uuid}/edit`);
}
