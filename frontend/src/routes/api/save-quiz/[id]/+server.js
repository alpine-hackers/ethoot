import API from "$lib/api";

export async function POST({ cookies, request, params }) {
    const newData = await request.json()

    await API.updateQuiz(cookies.get("session"), params.id, newData);

    return new Response(`{"status":"ok"}`, {
        status: 200,
        headers: {
            "Content-Type": "application/json"
        }
    });
}
