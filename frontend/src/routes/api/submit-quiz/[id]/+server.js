import API from "$lib/api";

export async function POST({ cookies, request, params }) {
    const userData = await request.json()

    const resp = await API.submitQuiz(cookies.get("session"), params.id, userData);

    return new Response(resp, {
        status: 200,
        headers: {
            "Content-Type": "application/json"
        }
    });
}
