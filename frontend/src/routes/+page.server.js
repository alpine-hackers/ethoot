import API from "$lib/api"

export async function load({ cookies }) {
	return {
        data: await API.loadQuizList(cookies.get("session"))
    };
}
