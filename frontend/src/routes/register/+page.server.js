import { fail, redirect } from "@sveltejs/kit";
import API from "$lib/api";

export const actions = {
	register: async ({ cookies, request }) => {
        const formData = await request.formData();

        if (!formData.has("username") || !formData.has("password")) {
            return fail(400, "Missing username or password");
        }

        const username = formData.get("username");
        const password = formData.get("password");

        console.log(username, password);

        const sessionToken = await API.register(username, password);
        
        if (!sessionToken) {
            return fail(401, "Invalid username or password");
        }

        cookies.set("session", sessionToken, {
            path: "/",
            httpOnly: true,
            maxAge: 60 * 60 * 24 * 7 // 1 week
        });
        
        throw redirect(302, "/");
	}
};
