from flask import Flask, request, session
from werkzeug.security import check_password_hash, generate_password_hash
import redis
import json
import uuid
from fuzzywuzzy import fuzz

DEBUG = False

redis_host: str =  '18.157.167.40' if not DEBUG else 'localhost'
db = redis.Redis(host=redis_host, port=6379, decode_responses=True, password='Yp6KSC9NFVGmZi')
print(f"using {redis_host} as redis data base")

app = Flask(__name__)
app.secret_key = b'406d062b9483472f1c0c679a4138a1e131333962c2a38f0e7d77158d5080971b'  # ¯\_(ツ)_/¯


def create_uuid(prefix: str) -> str:
    while True:
        id: str = str(uuid.uuid4())
        if not db.keys(f"{prefix}:{id}*"):
            return id


@app.post("/api/register")
def register():
    content: dict = request.json
    username = content['username']
    password = content['password']

    if not username:
        return {'success': False, 'user_id': '', 'reason': 'Username is required'}, 400
    elif not password:
        return {'success': False, 'user_id': '', 'reason': 'Incorrect username'}, 400


    if db.sismember('usernames', username):
        return {'success': False, 'user_id': '', 'reason': 'Username already exists'}, 400

    user_id = create_uuid('user')
    db.set(f'username:{username}:user_id', user_id)
    db.set(f'user:{user_id}:username', username)
    db.set(f'user:{user_id}:password_hash', generate_password_hash(password))

    return {'success': True, 'user_id': user_id, 'reason': 'OK'}, 200


@app.post("/api/login")
def login():
    content: dict = request.json
    username = content['username']
    password = content['password']

    if not username:
        return {'success': False, 'session_id': '', 'reason': 'Username is required'}, 400
    elif not password:
        return {'success': False, 'session_id': '', 'reason': 'Incorrect username'}, 400

    user_id = db.get(f'username:{username}:user_id')
    if user_id is None:
        return {'success': False, 'session_id': '', 'reason': 'Password is required'}, 400

    password_hash = db.get(f'user:{user_id}:password_hash')
    if password_hash is None or not check_password_hash(password_hash, password):
        return {'success': False, 'session_id': '', 'reason': 'Incorrect password'}, 400

    session.clear()
    session_id = create_uuid('session')
    db.set(f"session:{session_id}:user_id", user_id)
    session['session_id'] = session_id

    return {'success': True, 'session_id': session_id, 'reason': 'OK'}


@app.post("/api/loggedin")
def loggedin():
    session_id = request.json.get('session_id', "")
    if not db.exists(f'session:{session_id}:user_id'):
        return {'success': False, 'reason': "Not logged in"}, 403
    return {'success': True, 'reason': "OK"}, 200


@app.get('/logout')
def logout():
    session_id = request.json.get('session_id', "")
    db.delete(f'session:{session_id}:user_id')
    return "OK"


@app.route("/api/quiz/all")
def quiz_list_all():
    quizInfoList = []

    for quizKey in db.keys('quiz:*'):
        quiz = json.loads(db.get(quizKey))
        print('author', quiz['author'])
        author_name = db.get(f"user:{quiz['author']}:username")

        quizInfo = {}

        quizInfo["title"] = quiz["title"]
        quizInfo["id"] = quiz["id"]
        quizInfo["authorName"] = author_name
        quizInfo["tags"] = quiz["tags"]
        quizInfo["count"] = str(len(quiz["questions"]))

        quizInfoList.append(quizInfo)

    return quizInfoList


# Quiz

def create_question():
    question_id = create_uuid('question')
    question_default = {
        'question_id': question_id,
        'type': "MC",  # "MC", "SC", "INPUT"
        "title": "Intriguing Question Title",
        "text":
"""Is \\\\(P = NP\\\\)
```py
print('Hello World!')
```""",
        "choices": ['True', 'False'],  # list of strings describing choices
        "correct": [0],  # list of indeces of the correct choices
        "answer": "Hello World!",  # string comparision
    }
    
    question_json = json.dumps(question_default)
    db.set(f"question:{question_id}", question_json)

    return question_id

@app.post("/api/quiz/create")
def quiz_create():
    session_id = request.json.get('session_id', "")
    if not db.exists(f'session:{session_id}:user_id'):
        return {'success': False, 'reason': "Not logged in"}, 403

    user_id = db.get(f'session:{session_id}:user_id')
    if not user_id:
        return "Invalid User", 400

    quiz_id: str = create_uuid('quiz')

    qid = create_question()

    quiz_default = {
        'id': quiz_id,
        'title': 'Amazing Quiz Title',
        'description': 'This is an amazing quiz!',
        'author': user_id,
        'questions': [qid],
        'tags': [],
    }

    quiz_json = json.dumps(quiz_default)
    db.set(f"quiz:{quiz_id}", quiz_json)
    return quiz_id


@app.post("/api/quiz/<uuid:quiz_id>/update")
def quiz_update(quiz_id: uuid.UUID):
    content: dict = request.json

    for question_dict in content.get('questions', []):
        question_id = create_uuid('question')
        question_dict['question_id'] = question_id
        db.set(f"question:{question_id}", json.dumps(question_dict))

    content['questions'] = [question_dict['question_id'] for question_dict in content.get('questions', [])]
    db.set(f'quiz:{str(quiz_id)}', json.dumps(content))

    return { 'status': 'ok' }


@app.get("/api/quiz/<uuid:quiz_id>")
def quiz_get(quiz_id: uuid.UUID):
    data = db.get(f'quiz:{quiz_id}')
    
    if not data:
        return "<h1>Quiz not found</h1>", 404
    
    response = json.loads(data)

    for i in range(len(response["questions"])):
        qid = response["questions"][i]
        response["questions"][i] = json.loads(db.get(f"question:{qid}"))

    return response

@app.get("/api/quiz/<uuid:quiz_id>/question/<int:n>")
def quiz_get_question_n(quiz_id: uuid.UUID, n):
    quiz = db.get(f'quiz:{quiz_id}')
    question = {}

    if not quiz:
        return "<h1>Quiz not found</h1>", 404    

    quiz = json.loads(quiz)

    if n < 0 or n > len(quiz["questions"]):
        return "<h1>Question index out of bounds</h1>", 404

    question = db.get(f'question:{quiz["questions"][n - 1]}')

    if not question:        
        return "<h1>Question not found</h1>", 404    

    return json.loads(question)

# Questions

@app.post("/api/question/create")
def question_create():
    question_id = create_uuid('question')
    question_default = {
        'question_id': question_id,
        'type': "MC",  # "MC", "SC", "INPUT"
        "title": "Intriguing Question Title",
        "text": """Is $P = NP$?\n```py\nprint('Hello World!')\n```""",
        "choices": ['True', 'False'],  # list of strings describing choices
        "correct": [0],  # list of indeces of the correct choices
        "answer": "Hello World!",  # string comparision
    }

    question_json = json.dumps(question_default)
    db.set(f"question:{question_id}", question_json)
    return question_json


@app.post("/api/question/<uuid:question_id>/update")
def question_update(question_id: uuid.UUID):
    content: dict = request.json
    db.set(f'question:{str(question_id)}', json.dumps(content))
    return "OK"


@app.get("/api/question/<uuid:question_id>")
def question_get(question_id: uuid.UUID):
    data = db.get(f'question:{question_id}')
    if data:
        data_dict =  json.loads(data)
        #data_dict.pop('correct')
        #data_dict.pop('answer')
        return data_dict
    else:
        return "<h1>Question not found</h1>", 404


# @app.post("/api/question/<uuid:question_id>/verify")
# def question_verify(question_id: uuid.UUID):
#     question_json = db.get(f'question:{question_id}')
#     if not question_json:
#         return "<h1>Question not found</h1>", 404
#     question_dict = json.loads(question_json)

#     user_answer = request.json.get('answer', None)
#     our_answer = question_dict.get('answer', None)
#     if user_answer is not None and our_answer is not None:
#         return str(fuzz.ratio(user_answer.lower(), our_answer.lower()) > 75)

#     user_correct = request.json.get('correct', None)
#     our_correct = question_dict.get('correct', None)
#     if user_correct is not None and our_correct is not None:
#         return str({str(c) for c in user_correct} == {str(c) for c in our_correct})

#     return str(False)

@app.post("/api/quiz/<uuid:quiz_id>/verify")
def quiz_verify(quiz_id: uuid.UUID):
    quiz_json = db.get(f'quiz:{quiz_id}')
    if not quiz_json:
        return "<h1>Quiz not found</h1>", 404
    quiz_dict = json.loads(quiz_json)

    score = 0
    total = len(quiz_dict['questions'])
    
    user_answers = request.json

    print('ua:', user_answers)

    for i in range(len(quiz_dict['questions'])):
        qid = quiz_dict['questions'][i]
        question_dict = json.loads(db.get(f"question:{qid}"))

        user_answer = user_answers.get(str(i + 1), None) if len(user_answers) > i else None
        print(user_answer)

        if question_dict.get('type', None) == 'INPUT':
            our_answer = question_dict.get('answer', None)
            if user_answer is None:
                continue
            print(question_dict)
            if fuzz.ratio(user_answer.lower(), our_answer.lower()) > 75:
                score += 1
        else:
            our_correct = question_dict.get('correct', None)
            if user_answer is None:
                continue
            if {str(c) for c in user_answer} == {str(c) for c in our_correct}:
                score += 1

    return { 'correct': score, 'total': total }


@app.route("/api/test/<uuid:id>")
def test(id: str):
    """method for quick testing on the API"""
    print(id, type(id))
    return "OK"
