# ETHoot!

## Set up frontend

```
$ cd frontend
$ npm i
$ npm run dev
```

## Databse Setup

We use [Redis](https://redis.io)

### Installation

1. [ ] install redis for your operating system on https://redis.io/docs/getting-started/
2. [ ] `pip install redis` on your terminal

### Usage

#### Python

See https://redis.io/docs/clients/python/ for more info and docs.

minimal Redis and Flask example

```py
import redis
from flask import Flask

r = redis.Redis(host='localhost', port=6379, decode_responses=True)
r.set('foo', 'bar')

app = Flask(__name__)

@app.route("/")
def hello_world():
    return f"<p>{r.get('foo')}</p>"
```


### Terminal

Data should be persistent, even if flask is not running.

With `redis-cli` you can change and view data outside of Flask.

See here for more info: https://redis.io/docs/ui/cli/

Basic Commands:

```bash
$> redis-cli
127.0.0.1:6379> SET mykey "Hello\nWorld"
OK
127.0.0.1:6379> GET mykey
Hello
World
```


### Online Playground

If you want to learn or try out stuff or command without installing:
https://try.redis.io/


### 15min Redis Introduction

https://redis.io/docs/data-types/
