https://hackmd.io/Cj8T-QTES8aNsQZc1rtHAg?both

# Alpine Hackers (Team 11)

not focus on flashcards

## Tasks & Schedule
- ~~MVP defined~~
- ~~Schedule defined~~
- [x] minimal setup to start coding

## Responsibilities

Liam: Frontend programming
Vennilah: UX design
Peer/Bibin: Backend programming

## Core Problems or Values?
1. Making learning more **interactive**.
2. **collaboration**, learn with others
3. **Fun**, motivation for learning
4. **availability**
    - easy access from everywhere

## Core functionality

### quizzes

#### question types
- multiple choice questions
    - one question, several answers, multiple are correct
    - one question, several answers, one is correct
    - yes/no question
- question with answer
    - a (string) answer is reqired
    - number

### taking quizzes
- ~~filter quizes~~

### create quizes


## Features

- [x] no account needed for taking quizzes
- [x] score / result summary
- ~~leaderboard~~
- ~~profile picture~~
- ~~like, downvotes~~
- ~~comments~~
- ~~competition or tournaments~~
- do not award answering quickly
    - not a fast time limit like Kahoot!
- [x] quiz editors
- [x] **code and LaTeX support**
    - $f(\frac{a_i}{b^2} \le 42)$
    - ```py
      x : int = 3; print("Hello world!")
      ```
- [x] QR code or PIN code generator to join

## Presentation Idea
- list our stack
    - Svelete
    - Flask
    - Redis

```
VERSION = 0.1


```
